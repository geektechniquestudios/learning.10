package com.geektechnique.thymleafpractice.thymleafexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThymleafexerciseApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThymleafexerciseApplication.class, args);
    }

}
